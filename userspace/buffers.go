package userspace

import "time"

type LastContentState struct {
	UpdateAt         time.Time `json:"updateAt"`
	LastTag          Tag       `json:"lastTag"`
	AvailableActions []string  `json:"availableActions"`
}

type ActualContentState struct {
	IsChange bool     `json:"isChange"`
	Tag      Tag      `json:"newTag,omitempty"`
	Actions  []Action `json:"actions,omitempty"`
}

type Tag string

type Action struct {
	SenderName string  `json:"sender"`
	Content    Content `json:"content"`
}

type Content string
