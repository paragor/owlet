package userspace

import (
	"log"
	"os/exec"
	"errors"
	"bytes"
)

// [+] Отвечает за взаимодействие с пользовательскими файлами "send*"

func Send(path string, content Content) {
	errorPrefix := "userspace/Sender: program: '" + path + "'. "
	cmd := exec.Command(path, string(content))
	var errorBuffer bytes.Buffer
	cmd.Stderr = &errorBuffer

	err := cmd.Run()
	if err != nil {
		log.Println(errors.New(errorPrefix + " exit with '" + err.Error() + "' stdout:" + errorBuffer.String()))
	}
}
