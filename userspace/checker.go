package userspace

import (
	"encoding/json"
	"os/exec"
	"errors"
	"bytes"
)

// [+] Отвечает за взаимодействие с пользовательскими файлами "checkout*"

func Checkout(path string, req LastContentState) (ActualContentState, error) {
	jsonRequest, err := json.Marshal(req)
	errorPrefix := "userspace/Checkout: program: '" + path +  "'. "
	if err != nil {
		return ActualContentState{}, errors.New(errorPrefix + err.Error())
	}

	cmd := exec.Command(path, string(jsonRequest))
	var errorBuffer bytes.Buffer
	cmd.Stderr = &errorBuffer

	out, err := cmd.Output()
	if err != nil {
		return ActualContentState{}, errors.New(errorPrefix + " exit with error: '" + err.Error() + "' and stderr: " + errorBuffer.String())
	}

	var response ActualContentState
	err = json.Unmarshal(out, &response)
	if err != nil {
		return ActualContentState{}, errors.New(errorPrefix + err.Error())
	}
	return response, nil
}
