package main

import (
	"log"
	"time"
	"os"
	"encoding/json"
	"io/ioutil"
	"github.com/kalinina72/owlet/utils"
	"fmt"
	"path/filepath"
	"path"
)

func main() {

	log.SetFlags(log.Lshortfile | log.Ldate | log.Ltime)

	exFile, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	dir := filepath.Dir(exFile)

	f, err := os.Open(path.Join(dir, "config.json"))
	if err != nil {
		log.Println("Error config read:")
		log.Fatal(err)
	}
	defer f.Close()

	bytes, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}

	var config utils.Config

	json.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	resolver := &utils.Resolver{}

	err = resolver.ScanForCheckers(config.CheckersDir)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Found checkers: %v\n", resolver.GetCheckers())

	err = resolver.ScanForSenders(config.SendersDir)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Found senders: %v\n", resolver.GetSenders())

	saver, err := utils.MakeSqliteSaver(config.SqliteFilePath)
	if err != nil {
		log.Fatal(err)
	}

	periodOfChecks, err := time.ParseDuration(config.PeriodOfChecks)
	if err != nil {
		periodOfChecks = time.Minute * 5
		log.Println("main: Set default time, because occured err: " + err.Error())
	}

	// Main loop
	do := func() {
		for _, checkerName := range resolver.GetCheckers() {
			go utils.OwletWorker(checkerName, resolver, saver)
		}
	}

	timer := time.Tick(periodOfChecks)
	do()
	for range timer {
		do()
	}

}
