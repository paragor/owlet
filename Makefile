
HOME = $(shell echo ~)
USER = $(shell stat -c "%U" ~)
GROUP = $(shell stat -c "%G" ~)
ROOT_DIR = $(HOME)/.owlet
CHECKERS_DIR = $(ROOT_DIR)/checkers
SENDERS_DIR = $(ROOT_DIR)/senders
DB_FILE_PATH = $(ROOT_DIR)/content.db
CONFIG_PATH = $(ROOT_DIR)/config.json
EXEC_DIR = /usr/local/bin
BIN_FILE = owlet
config = "\
{\n\
  \"sqlite_file_path\": \"$(DB_FILE_PATH)\",\n\
  \"checkers_dir\": \"$(CHECKERS_DIR)\",\n\
  \"senders_dir\": \"$(SENDERS_DIR)\",\n\
  \"period_of_checks\": \"5m\"\n\
}\
"

all: build install

build:
	go get -fix
	go build -o $(BIN_FILE) main.go

install:
	go get -fix
	go build -o $(BIN_FILE) main.go
	-mkdir $(ROOT_DIR)
	-mkdir $(CHECKERS_DIR)
	-mkdir $(SENDERS_DIR)
	touch $(DB_FILE_PATH)
	echo $(config) > $(CONFIG_PATH)
	mv $(BIN_FILE) $(ROOT_DIR)/$(BIN_FILE)
	chown $(USER):$(GROUP) -R $(ROOT_DIR)
	ln -fs $(ROOT_DIR)/$(BIN_FILE) $(EXEC_DIR)/$(BIN_FILE)
	echo "\n\nSUCCESS installing!"

update:
	go get -fix
	go build -o $(BIN_FILE) main.go
	mv $(BIN_FILE) $(ROOT_DIR)/$(BIN_FILE)

remove:
	rm $(EXEC_DIR)/$(BIN_FILE)

purge: clean

clean:
	-rm -r $(ROOT_DIR)
	-rm $(EXEC_DIR)/$(BIN_FILE)
