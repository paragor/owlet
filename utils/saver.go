package utils

import (
	"time"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/kalinina72/owlet/userspace"
)

// [] Отвечает за связь с базой данных
// [] Сохраняет информацию от checkers
// [] Отдает информацию для checkers

type Saver interface {
	GetLastContentState(name string) (userspace.LastContentState, error)
	SaveContentState(name string, tag userspace.Tag) error
}

type SqliteSaver struct {
	db              *sql.DB
	getContentStmt  string
	saveContentStmt string
}

func MakeSqliteSaver(path string) (Saver, error) {
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	createStmt := `CREATE TABLE IF NOT EXISTS states (
		checkout_name varchar PRIMARY KEY ,
		tag text,
		updated_at datetime
	);
`
	_, err = db.Exec(createStmt)
	if err != nil {
		return nil, err
	}

	getContentStmt := "SELECT tag, updated_at FROM states WHERE checkout_name=?"
	saveContentStmt := "INSERT OR REPLACE INTO states (checkout_name, tag, updated_at) VALUES (?, ?, ?)"

	saver := &SqliteSaver{db: db, getContentStmt: getContentStmt, saveContentStmt: saveContentStmt}
	return saver, nil
}

func (s *SqliteSaver) GetLastContentState(name string) (userspace.LastContentState, error) {
	var tag string
	var updatedAt time.Time
	stmt, err := s.db.Prepare(s.getContentStmt)
	if err != nil {
		return userspace.LastContentState{}, err
	}
	defer stmt.Close()
	err = stmt.QueryRow(name).Scan(&tag, &updatedAt)
	if err == sql.ErrNoRows {
		return userspace.LastContentState{UpdateAt: time.Unix(0, 0), LastTag: userspace.Tag("")}, nil
	}
	return userspace.LastContentState{UpdateAt: updatedAt, LastTag: userspace.Tag(tag)}, err
}

func (s *SqliteSaver) SaveContentState(name string, tag userspace.Tag) error {
	stmt, err := s.db.Prepare(s.saveContentStmt)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(name, tag, time.Now())
	return err
}
