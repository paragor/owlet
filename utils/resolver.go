package utils

import (
	"errors"
	"io/ioutil"
	"log"
	"path"
	"strings"
)

// [+] Парсит "my_content" дирректории на на наличие "checkout*" файла.
// [+] Парсит "my_worker" дирректории на наличие "send*" файлов

type Resolver struct {
	checkers map[string]string
	senders  map[string]string
}

func (r *Resolver) GetCheckers() []string {
	res := make([]string, 0, len(r.checkers))
	for name := range r.checkers {
		res = append(res, name)
	}
	return res
}

func (r *Resolver) GetSenders() []string {
	res := make([]string, 0, len(r.senders))
	for name := range r.senders {
		res = append(res, name)
	}
	return res
}

func (r *Resolver) ResolveChecker(name string) (string, error) {
	if cPath, ok := r.checkers[name]; ok {
		return cPath, nil
	}
	return "", errors.New("utils/resolver: Don`t know checker with name:" + name)
}

func (r *Resolver) ResolveSender(name string) (string, error) {
	if sPath, ok := r.senders[name]; ok {
		return sPath, nil
	}
	return "", errors.New("utils/resolver: Don`t know sender with name: " + name)
}

func (r *Resolver) scanFor(dir string, searchPrefix string) (map[string]string, error) {

	checkers := make(map[string]string)

	checkersDir, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	for _, checkerDir := range checkersDir {
		if !checkerDir.IsDir() {
			continue
		}

		if files, err := ioutil.ReadDir(path.Join(dir, checkerDir.Name())); err == nil {
			for _, file := range files {
				if strings.HasPrefix(file.Name(), searchPrefix) {
					checkers[checkerDir.Name()] = path.Join(dir, checkerDir.Name(), file.Name())
				}
			}
		} else {
			log.Println(err)
			continue
		}

	}
	if len(checkers) == 0 {
		return checkers, errors.New("utils/resolver: No one file with prefix '" + searchPrefix + "' in " + dir)
	}
	return checkers, nil
}

func (r *Resolver) ScanForCheckers(dir string) error {
	checkers, err := r.scanFor(dir, "checkout")
	if err == nil {
		r.checkers = checkers
	}

	return err

}

func (r *Resolver) ScanForSenders(dir string) error {
	senders, err := r.scanFor(dir, "send")
	if err == nil {
		r.senders = senders
	}

	return err
}
