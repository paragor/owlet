package utils

import (
	"log"
	"github.com/kalinina72/owlet/userspace"
)

// [+] Раздает задачи sender`у и checker`у

func OwletWorker(checkerName string, resolver *Resolver, saver Saver) {
	checkerPath, err := resolver.ResolveChecker(checkerName)
	if err != nil {
		log.Println(err)
		return
	}

	lastContentState, err := saver.GetLastContentState(checkerName)
	if err != nil {
		log.Println(err)
		return
	}
	lastContentState.AvailableActions = resolver.GetSenders()
	actualContent, err := userspace.Checkout(checkerPath, lastContentState)
	if err != nil {
		log.Println(err)
		return
	}

	if actualContent.IsChange {
		saver.SaveContentState(checkerName, actualContent.Tag)
		if len(actualContent.Actions) == 0 {
			return
		}

		for _, action := range actualContent.Actions {
			senderPath, err := resolver.ResolveSender(action.SenderName)
			if err != nil {
				log.Println(err)
				continue
			}

			go userspace.Send(senderPath, action.Content)
		}
	}

}
