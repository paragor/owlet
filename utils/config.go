package utils

type Config struct {
	CheckersDir    string `json:"checkers_dir"`
	SendersDir     string `json:"senders_dir"`
	SqliteFilePath string `json:"sqlite_file_path"`
	PeriodOfChecks string `json:"period_of_checks"` //example: 5m3s
}
